import re, os
from flask import Flask, render_template
app = Flask(__name__)

w1_sys_dir = '/sys/bus/w1/devices/'
slaves_file = os.path.join(w1_sys_dir, 'w1_bus_master1', 'w1_master_slaves')
regex_crc_line = re.compile(r'^(?P<raw>([0-9a-f]{2}\s){9}):\scrc=(?P<crc>[a-f0-9]{2})\s(?P<crc_good>.+)$')
regex_data_line = re.compile(r'^(?P<raw>([0-9a-f]{2}\s){9})(?P<data>.+)$')

def get_w1_data_dict(slave):
    d = {}
    lines = [line.rstrip() for line in open(os.path.join(w1_sys_dir, slave, 'w1_slave'))]
    for line in lines:
        match_crc_line = re.match(regex_crc_line, line)
        if (match_crc_line):
            d['crc'] = match_crc_line.group('crc')
            d['crc_good'] = match_crc_line.group('crc_good')
            continue
            
        match_data_line = re.match(regex_data_line, line)
        if (match_data_line):
            d['raw'] = match_data_line.group('raw')
            d['data'] = match_data_line.group('data')
    return d
    

def scan_w1_bus():
    d = {}
    if not os.path.isfile(slaves_file):
        return d
    
    w1_slaves = [line.rstrip() for line in open(slaves_file)]
    for slave in w1_slaves:
        d[slave] = get_w1_data_dict(slave)

    return d


@app.route("/")
def start():
    return render_template('table.html.j2', d=scan_w1_bus())

if __name__ == "__main__":
#     app.debug = True
    app.run(host = '0.0.0.0')

        